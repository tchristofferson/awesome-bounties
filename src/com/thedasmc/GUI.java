package com.thedasmc;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GUI implements Listener {

    private List<Inventory> inventories;
    private Bounty plugin;
    private Manager manager;

    public GUI(Bounty plugin, Manager manager) {

        inventories = new ArrayList<>();
        this.plugin = plugin;
        this.manager = manager;

    }

    //return -1 if the inv is not in inventories list
    private int positionInInventories(Inventory inventory) {

        int i = 0;

        for (Inventory inv : inventories) {

            if (Arrays.equals(inv.getContents(), inventory.getContents())) {

                return i;

            }

            i += 1;

        }

        return -1;

    }

    private void addHeadsToInventories() {

        List<String> playerNames = manager.getPlayersWithBounty();
        //item stacks for navigation
        //close menu button
        ItemStack close = new ItemStack(Material.BARRIER, 1);
        ItemMeta closeMeta = close.getItemMeta();
        closeMeta.setDisplayName(ChatColor.RED + "Close Menu");
        close.setItemMeta(closeMeta);
        //next page button
        ItemStack next = createSkull("MHF_ArrowRight");
        SkullMeta nextMeta = (SkullMeta) next.getItemMeta();
        nextMeta.setOwner("MHF_ArrowRight");
        nextMeta.setDisplayName(ChatColor.GOLD + "Next Page >>");
        next.setItemMeta(nextMeta);
        //previous page button
        ItemStack previous = createSkull("MHF_ArrowLeft");
        SkullMeta previousMeta = (SkullMeta) previous.getItemMeta();
        previousMeta.setOwner("MHF_ArrowLeft");
        previousMeta.setDisplayName(ChatColor.GOLD + "<< Previous Page");
        previous.setItemMeta(previousMeta);

        List<Inventory> inventoryList = new ArrayList<>();

        for (Inventory inv : inventories) {

            if (inv.getSize() == 54) {

                Inventory inventory = Bukkit.createInventory(null, 54, ChatColor.GOLD + "Bounties");

                for (int x = 0; x < 45; x++) {

                    if (!playerNames.isEmpty()) {

                        String playerName = playerNames.get(0);
                        inventory.setItem(x, createBountySkull(playerName, manager.getBounty(playerName)));
                        playerNames.remove(0);

                    } else {

                        break;

                    }

                }

                inventory.setItem(53, next);//next page button
                inventory.setItem(49, close);//close inv button
                inventory.setItem(45, previous);//previous page button
                inventoryList.add(inventory);

            } else {

                int invSize = inv.getSize();
                Inventory inventory = Bukkit.createInventory(null, invSize, ChatColor.GOLD + "Bounties");
                playerNames.forEach(name -> inventory.addItem(createBountySkull(name, manager.getBounty(name))));
                inventoryList.add(inventory);

            }

        }

        inventories = inventoryList;

    }

    private void createInventories() {

        Inventory mainInv;
        int numBounties = manager.getBounties().size();
        int slots;

        if (numBounties % 9 == 0 && numBounties < 46) {

            slots = numBounties;
            mainInv = Bukkit.createInventory(null, slots, ChatColor.GOLD + "Bounties");

        } else if (numBounties < 9) {

            slots = 9;
            mainInv = Bukkit.createInventory(null, slots, ChatColor.GOLD + "Bounties");

        } else {

            int remainder = numBounties % 9;
            slots = numBounties + (9 - remainder);
            mainInv = Bukkit.createInventory(null, slots, ChatColor.GOLD + "Bounties");

        }

        if (slots > 45) {

            int neededInventories = (int) Math.ceil(numBounties / 45.0);

            Inventory inv = Bukkit.createInventory(null, 54, ChatColor.GOLD + "Bounties");

            for (int i = 0; i < neededInventories; i++) {

                inventories.add(inv);

            }

            addHeadsToInventories();

        } else {

            inventories.add(mainInv);
            addHeadsToInventories();

        }

    }

    private ItemStack createBountySkull(String playerName, Double bounty) {

        ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta  headMeta = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
        List<String> headLore = new ArrayList<>();

        headLore.add(ChatColor.GREEN + "$" + bounty);
        headMeta.setOwner(playerName);
        headMeta.setDisplayName(ChatColor.BOLD + playerName);

        headMeta.setLore(headLore);
        head.setItemMeta(headMeta);

        return head;

    }

    private ItemStack createSkull(String playerName) {

        ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta  headMeta = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);

        headMeta.setOwner(playerName);

        return head;

    }

    //TODO: Multi page gui
    public void open(Player player, int index) {

        createInventories();
        Inventory inv = inventories.get(index);
        player.openInventory(inv);

    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {

        if (ChatColor.stripColor(event.getInventory().getName()).equalsIgnoreCase("Bounties")) {

            event.setCancelled(true);
            Player player = (Player) event.getWhoClicked();

            ItemStack item = event.getCurrentItem();

            Inventory inv = event.getInventory();
            int invSize = inventories.size();//How many inventories there are (not slots)

            if (item.getType().equals(Material.SKULL_ITEM)) {

                SkullMeta itemMeta = (SkullMeta) item.getItemMeta();

                if (itemMeta.getOwner().equalsIgnoreCase("MHF_ArrowLeft")) {

                    System.out.println("[Bounty] index in inventories: " + positionInInventories(inv));

                    if (positionInInventories(inv) != 0) {

                        System.out.println("[Bounty] clicked item's position is not 0");

                        if (positionInInventories(inv) == -1) {

                            System.out.println("[Bounty] positionInInventories(inv) = -1");
                            return;

                        }

                        player.closeInventory();
                        open(player, positionInInventories(inv) - 1);

                    }

                } else if (itemMeta.getOwner().equalsIgnoreCase("MHF_ArrowRight")) {

                    System.out.println("[Bounty] index in inventories: " + positionInInventories(inv));

                    if (positionInInventories(inv) < invSize - 1) {

                        if (positionInInventories(inv) == -1) {

                            System.out.println("[Bounty] positionInInventories(inv) = -1");
                            return;

                        }

                        player.closeInventory();
                        open(player, positionInInventories(inv) + 1);

                    }


                }

            } else {

                player.closeInventory();
                player.sendMessage(ChatColor.RED + "Closed Menu!");

            }

        }

    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {

        Player victim = event.getEntity().getPlayer();
        Player killer = event.getEntity().getKiller();

        if (killer != null) {

            if (manager.hasBounty(victim.getName())) {

                Double bounty = manager.getBounty(victim.getName());

                manager.removeBounty(victim.getName());
                plugin.econ.depositPlayer(killer, bounty);
                killer.sendMessage(ChatColor.GOLD + "[Bounty] You killed a player with a bounty of " + ChatColor.GREEN + "$" + bounty + ChatColor.GOLD +
                        "!\nThe money has been deposited into your account!");
                victim.sendMessage(ChatColor.GOLD + "[Bounty] Your bounty has been removed!");

            }

        }

    }

}
