package com.thedasmc;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;
import java.util.logging.Logger;

public class Bounty extends JavaPlugin {

    private GUI gui;
    private Manager manager;
    private static final Logger log = Logger.getLogger("Minecraft");
    public Economy econ = null;
    private PluginManager pm;
    //Permissions
    //TODO: Use permissions
    private Permission removePermission;
    private Permission setPermission;
    private Permission listPermission;
    private Permission checkPermission;
    private Permission increasePermission;
    private Permission allPermission;

    @Override
    public void onEnable() {

        getConfig().options().copyDefaults(true);

        if (!setupEconomy() ) {

            log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;

        }

        pm = Bukkit.getPluginManager();
        manager = new Manager(this, econ);
        gui = new GUI(this, manager);
        pm.registerEvents(manager, this);
        pm.registerEvents(gui, this);
        removePermission = new Permission("bounty.remove");
        setPermission = new Permission("bounty.set");
        listPermission = new Permission("bounty.list");
        checkPermission = new Permission("bounty.check");
        increasePermission = new Permission("bounty.increase");
        allPermission = new Permission("bounty.*");
        manager.loadBounties();
        saveConfig();

    }

    @Override
    public void onDisable() {

        manager.saveBounties();
        saveConfig();

    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {

            Player player = (Player) sender;

            if (command.getName().equalsIgnoreCase("bounty")) {

                if (args.length > 0) {

                    switch (args[0].toLowerCase()) {

                        case "set":
                            if (player.hasPermission(setPermission) || player.hasPermission(allPermission) || player.isOp()) {

                                if (args.length == 3) {

                                    if (args[1].equalsIgnoreCase(player.getName())) {

                                        player.sendMessage(ChatColor.RED + "You cannot put a bounty on yourself!");
                                        return false;

                                    }

                                    Player gettingBounty = getOnlinePlayer(args[1]);

                                    if (gettingBounty != null) {

                                        if (!manager.hasBounty(args[1])) {

                                            try {

                                                Double bounty = Double.parseDouble(args[2]);

                                                if (econ.getBalance(player) >= bounty) {

                                                    econ.withdrawPlayer(player, bounty);
                                                    manager.addBounty(args[1], bounty);
                                                    player.sendMessage(ChatColor.GOLD + "Bounty set!");
                                                    getServer().broadcastMessage(ChatColor.GOLD + "[Bounty] " + player.getName() +
                                                            " has put a bounty of " + ChatColor.GREEN + "$" + bounty + ChatColor.GOLD + " on " + gettingBounty.getName());

                                                } else {

                                                    player.sendMessage(ChatColor.RED + "Insufficient Funds!");

                                                }

                                            } catch (IllegalArgumentException iae) {

                                                player.sendMessage(ChatColor.RED + "Invalid amount!\nUsage: /Bounty set <player> <amount>");

                                            }

                                        } else {

                                            player.sendMessage(ChatColor.RED + gettingBounty.getName() + " already has a bounty!\nIf you want to increase the bounty type /Bounty increase <player>");

                                        }

                                    } else {

                                        player.sendMessage(ChatColor.RED + "Player Not Found!");

                                    }

                                } else {

                                    player.sendMessage(ChatColor.RED + "Usage: /Bounty set <player> <amount>");

                                }

                            } else {

                                player.sendMessage(ChatColor.RED + "Permission Denied!");

                            }
                            break;
                        case "remove":
                            if (player.hasPermission(removePermission) || player.hasPermission(allPermission) || player.isOp()) {

                                if (args.length == 2) {

                                    if (manager.hasBounty(args[1])) {

                                        manager.removeBounty(args[1]);
                                        player.sendMessage(ChatColor.GOLD + args[1] + "'s Bounty Removed!");

                                    } else {

                                        player.sendMessage(ChatColor.RED + "That player does not have a bounty!");

                                    }

                                } else {

                                    player.sendMessage(ChatColor.RED + "Usage: /Bounty remove <player>");

                                }

                            } else {

                                player.sendMessage(ChatColor.RED + "Permission Denied!");

                            }
                            break;
                        case "list":
                            gui.open(player, 0);
                            break;
                        case "check":
                            if (player.hasPermission(checkPermission) || player.hasPermission(allPermission) || player.isOp()) {

                                if (args.length == 2) {

                                    if (manager.hasBounty(args[1])) {

                                        player.sendMessage(ChatColor.GOLD + args[1] + " has a bounty of " + ChatColor.GREEN + "$" + manager.getBounty(args[1]));

                                    } else {

                                        player.sendMessage(ChatColor.GOLD + args[1] + " does not have a bounty!");

                                    }

                                } else {

                                    player.sendMessage(ChatColor.RED + "Usage: /Bounty check <player>");

                                }

                            } else {

                                player.sendMessage(ChatColor.RED + "Permission Denied!");

                            }
                            break;
                        case "increase":
                            if (player.hasPermission(increasePermission) || player.hasPermission(allPermission) || player.isOp()) {

                                if (args.length == 3) {

                                    if (manager.hasBounty(args[1])) {

                                        try {

                                            Double increaseAmount = Double.parseDouble(args[2]);

                                            econ.withdrawPlayer(player, increaseAmount);
                                            manager.updateBounty(args[1], increaseAmount);
                                            player.sendMessage(ChatColor.GREEN + args[1] + "'s bounty is now $" + manager.getBounty(args[1]));

                                        } catch (IllegalArgumentException iae) {

                                            player.sendMessage(ChatColor.RED + "Invalid Amount!");

                                        }

                                    } else {

                                        player.sendMessage(ChatColor.RED + "That player does not have a bounty\nUse /Bounty set instead!");

                                    }

                                } else {

                                    player.sendMessage(ChatColor.RED + "Usage: /Bounty increase <player> <increase amount>");

                                }

                            } else {

                                player.sendMessage(ChatColor.RED + "Permission Denied!");

                            }
                            break;
                        default:
                            player.sendMessage(ChatColor.RED + "Bounty Commands:\n/Bounty set <player> <amount> - Set a bounty on a player\n" +
                                    "/Bounty remove <player> - Removes a player's bounty\n" +
                                    "/Bounty list - Shows all active bounties\n" +
                                    "/Bounty check - Check if a player has a bounty\n" +
                                    "/Bounty increase - Increase a players bounty");
                            break;

                    }

                } else {

                    player.sendMessage(ChatColor.RED + "Bounty Commands:\n/Bounty set <player> <amount> - Set a bounty on a player\n" +
                            "/Bounty remove <player> - Removes a player's bounty\n" +
                            "/Bounty list - Shows all active bounties\n" +
                            "/Bounty check - Check if a player has a bounty\n" +
                            "/Bounty increase - Increase a players bounty");

                }

            }

        }

        return false;

    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    private Player getOnlinePlayer(String playerName) {

        for (Player player : Bukkit.getOnlinePlayers()) {

            if (player.getName().equalsIgnoreCase(playerName)) {

                return player;

            }

        }

        return null;

    }

}
