package com.thedasmc;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Manager implements Listener {

    private Bounty plugin;
    private Map<String, Double> bounties;
    private Economy econ;

    public Manager(Bounty plugin, Economy econ) {

        this.plugin = plugin;
        this.econ = econ;
        bounties = new HashMap<>();

    }

    public List<String> getPlayersWithBounty() {

        List<String> players = new ArrayList<>();

        players.addAll(bounties.keySet());

        return players;

    }

    public void saveBounties() {

        FileConfiguration config = plugin.getConfig();

        config.set("Bounties", null);

        bounties.keySet().forEach(name -> config.set("Bounties." + name + ".bounty", bounties.get(name)));

    }

    public void loadBounties() {

        FileConfiguration config = plugin.getConfig();

        if (config.contains("Bounties") && !config.getConfigurationSection("Bounties").getKeys(false).isEmpty()) {

            config.getConfigurationSection("Bounties").getKeys(false).forEach(name -> {
                Double bounty = config.getDouble("Bounties." + name + ".bounty");
                bounties.put(name, bounty);
            });

        }

    }

    public Map<String, Double> getBounties() {

        return bounties;

    }

    public boolean hasBounty(String playerName) {

        for (String name : bounties.keySet()) {

            if (name.equalsIgnoreCase(playerName)) {

                return true;

            }

        }

        return false;

    }

    public Double getBounty(String playerName) {

        for (String name : bounties.keySet()) {

            if (name.equalsIgnoreCase(playerName)) {

                return bounties.get(name);

            }

        }

        return null;

    }

    public void updateBounty(String playerName, Double amount) {

        bounties.keySet().forEach(name -> {
            if (name.equalsIgnoreCase(playerName)) {

                Double currentBounty = bounties.get(name);
                bounties.put(name, currentBounty + amount);

            }
        });

    }

    public void addBounty(String playerName, Double amount) {

        bounties.put(playerName, amount);

    }

    public void removeBounty(String playerName) {

        String name = "";

        for (String s : bounties.keySet()) {

            if (s.equalsIgnoreCase(playerName)) {

                name = s;
                break;

            }

        }

        bounties.remove(name);

    }

    @EventHandler
    public void onPlayerKilled(PlayerDeathEvent event) {

        Player killer = event.getEntity().getKiller();
        Player victim = event.getEntity().getPlayer();

        if (killer != null && killer.getType().equals(EntityType.PLAYER)) {

            System.out.println(killer.getName());

            if (hasBounty(victim.getName())) {

                if (!killer.getUniqueId().equals(victim.getUniqueId())) {

                    String victimName = victim.getName();

                    econ.depositPlayer(killer, getBounty(victimName));
                    killer.sendMessage(ChatColor.GOLD + "[Bounty] You killed a player with a bounty and received " + ChatColor.GREEN + "$" + getBounty(victimName));
                    removeBounty(victimName);
                    victim.sendMessage(ChatColor.GOLD + "[Bounty] You bounty has been removed!");

                }

            }

        }

    }

}
